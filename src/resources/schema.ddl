create database traffic_camera_reports;

use traffic_camera_reports;

create table reports(location_name varchar(255) NOT NULL, camera_status varchar(255) NOT NULL, camera_mfg varchar(255) NOT NULL, atd_location_id varchar(255) NOT NULL, signal_end_area varchar(255) NOT NULL, council_district varchar(255) NOT NULL, camera_id int NOT NULL AUTO_INCREMENT, PRIMARY KEY(camera_id));