package assign.services;

import assign.domain.Report;
import assign.domain.Reports;

import java.util.List;


public interface TrafficCameraReportsService {

	//POST
	public Report addReport(Report report) throws Exception;
	
	//GET
	public Report getReport(int cameraId) throws Exception;

	//PUT
    public Report updateReport(Report updatedReport, int cameraId) throws Exception;
    
    //DELETE
    public boolean deleteReport(int cameraId) throws Exception;
    
    public List<Report> getAllReports() throws Exception; 
    
    public List<Report> getAllReportsByStatus(String cameraStatus) throws Exception;

}
