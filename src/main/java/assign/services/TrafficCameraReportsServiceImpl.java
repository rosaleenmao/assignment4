package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.Report;
import assign.domain.Reports;

public class TrafficCameraReportsServiceImpl implements TrafficCameraReportsService {

	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;

	// DB connection information would typically be read from a config file.
	public TrafficCameraReportsServiceImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUsername("root");
        ds.setPassword("computer");
        ds.setUrl("jdbc:mysql://localhost:3306/traffic_camera_reports");
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        return ds;
    }
	
	@Override
	public Report addReport(Report report) throws Exception {
		Connection conn = ds.getConnection();
		
		String insert = "INSERT INTO "
				+ "reports(location_name, camera_status, camera_mfg, atd_location_id, signal_end_area, council_district) "
				+ "VALUES(?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, report.getLocationName());
		stmt.setString(2, report.getCameraStatus());
		stmt.setString(3, report.getCameraMfg());
		stmt.setString(4, report.getAtdLocationId());
		stmt.setString(5, report.getSignalEndArea());
		stmt.setString(6, report.getCouncilDistrict());
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating course failed, no rows affected.");
        }
        
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
        	report.setCameraId(generatedKeys.getInt(1));
        }
        else {
            throw new SQLException("Creating course failed, no ID obtained.");
        }
        
        // Close the connection
        conn.close();
        
		return report;
	}
	
	//return null if camera with given cameraId does not exist
	@Override
	public Report updateReport(Report updatedReport, int cameraId) throws Exception {
		Connection conn = ds.getConnection();
		
		String update = "UPDATE reports "
				+ "SET location_name = ?, camera_status = ?, camera_mfg = ?, "
				+ "atd_location_id = ?, signal_end_area = ?, council_district = ? "
				+ "WHERE camera_id = ?";
		PreparedStatement stmt = conn.prepareStatement(update);
		
		stmt.setString(1, updatedReport.getLocationName());
		stmt.setString(2, updatedReport.getCameraStatus());
		stmt.setString(3, updatedReport.getCameraMfg());
		stmt.setString(4, updatedReport.getAtdLocationId());
		stmt.setString(5, updatedReport.getSignalEndArea());
		stmt.setString(6, updatedReport.getCouncilDistrict());
		stmt.setLong(7, cameraId);
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            return null;
        }
        
        updatedReport.setCameraId(cameraId);
        
        // Close the connection
        conn.close();
        
		return updatedReport;
	}
	
	//return null if camera with given cameraId does not exist
	@Override
	public Report getReport(int cameraId) throws Exception {
		String query = "select * from reports where camera_id = ?";
		
		Connection conn = ds.getConnection();
		PreparedStatement s = conn.prepareStatement(query);
		
		s.setString(1, String.valueOf(cameraId));
		
		ResultSet r = s.executeQuery();
		
		if (!r.next()) {
		    return null;
		}
		
		Report report = new Report();
		report.setAtdLocationId(r.getString("atd_location_id"));
		report.setCameraId(cameraId);
		report.setCameraMfg(r.getString("camera_mfg"));
		report.setCameraStatus(r.getString("camera_status"));
		report.setCouncilDistrict(r.getString("council_district"));
		report.setLocationName(r.getString("location_name"));
		report.setSignalEndArea(r.getString("signal_end_area"));
		return report;
	}


	//return false if that cameraId isn't there
	@Override
	public boolean deleteReport(int cameraId) throws Exception {
		Connection conn = ds.getConnection();
		
		String delete = "DELETE from reports WHERE camera_id = ?";
		PreparedStatement stmt = conn.prepareStatement(delete);

		stmt.setString(1, String.valueOf(cameraId));
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            return false;
        }
        
        // Close the connection
        conn.close();
        
		return true;
		
	}

	@Override
	public List<Report> getAllReports() throws Exception {
		String query = "select camera_id from reports";
		
		Connection conn = ds.getConnection();
		PreparedStatement s = conn.prepareStatement(query);
		
		ResultSet r = s.executeQuery();
		
		List<Report> reports = new ArrayList<Report>();
		
		while (r.next()) {
			Report report = new Report();
			report.setCameraId((r.getInt("camera_id")));
			reports.add(report);
		}

		return reports;
	}

	//return empty list if you can't find it or if cameraStatus is null
	@Override
	public List<Report> getAllReportsByStatus(String cameraStatus) throws Exception {
		String query = "SELECT camera_id from reports WHERE camera_status = ?";
		
		Connection conn = ds.getConnection();
		PreparedStatement s = conn.prepareStatement(query);
		
		s.setString(1, cameraStatus);
		
		ResultSet r = s.executeQuery();
		
		List<Report> reports = new ArrayList<Report>();
		
		while (r.next()) {		
			Report report = new Report();
			report.setCameraId((r.getInt("camera_id")));
			reports.add(report);
		}

		return reports;
	}

}
