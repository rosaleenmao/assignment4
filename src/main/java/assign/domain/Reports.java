package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reports")
@XmlAccessorType
public class Reports {

    private List<Report> reports = null;
 

    @XmlElement(name = "report")
    public List<Report> getReports() {
        return reports;
    }
 
    public void setReports(List<Report> reports) {
        this.reports = reports;
    }	
}
