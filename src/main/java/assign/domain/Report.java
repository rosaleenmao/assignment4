package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "report")
public class Report {

	private String location_name;
	private String camera_status;
	private String camera_mfg;
	private String atd_location_id;
	private String signal_end_area;
	private String council_district;
	private int camera_id;

	@XmlElement(name="location_name")
	public String getLocationName() {
		return location_name;
	}

	@XmlElement(name="camera_status")
	public String getCameraStatus() {
		return camera_status;
	}

	@XmlElement(name="camera_mfg")
	public String getCameraMfg() {
		return camera_mfg;
	}

	@XmlElement(name="atd_location_id")
	public String getAtdLocationId() {
		return atd_location_id;
	}

	@XmlElement(name="signal_eng_area")
	public String getSignalEndArea() {
		return signal_end_area;
	}

	@XmlElement(name="council_district")
	public String getCouncilDistrict() {
		return council_district;
	}
	
	public void setLocationName(String location_name) {
		this.location_name = location_name;
	}
	
	public void setCameraStatus(String camera_status) {
		this.camera_status = camera_status;
	}
	
	public void setCameraMfg(String camera_mfg) {
		this.camera_mfg = camera_mfg;
	}
	
	public void setAtdLocationId(String atd_location_id) {
		this.atd_location_id = atd_location_id;
	}
	
	public void setSignalEndArea(String signal_end_area) {
		this.signal_end_area = signal_end_area;
	}
	
	public void setCouncilDistrict(String council_district) {
		this.council_district = council_district;
	}

	@XmlElement(name="camera_id")
	public int getCameraId() {
		return camera_id;
	}
	
	public void setCameraId(int camera_id) {
		this.camera_id = camera_id;
	}
}