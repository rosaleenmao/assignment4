package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Report;
import assign.domain.Reports;
import assign.services.TrafficCameraReportsService;
import assign.services.TrafficCameraReportsServiceImpl;

@Path("/")
public class TrafficCameraReportsResource {

	TrafficCameraReportsService trafficCameraReportsService;
	String password;
	String username;
	String dburl;
	String host;
	String dbname;

	public TrafficCameraReportsResource(@Context ServletContext servletContext) {
		host = servletContext.getInitParameter("DBHOST");
		dbname = servletContext.getInitParameter("DBNAME");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		//jdbc:mysql://localhost:3306/student_courses
        dburl = "jdbc:mysql://" + host + ":3306/" + dbname + "?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		this.trafficCameraReportsService = new TrafficCameraReportsServiceImpl(dburl, username, password);		
	}

	@GET
	@Path("/")
	@Produces("text/html")
	public String helloWorld() {		
		return "Welcome to Traffic Camera Reports REST API"
				+ "<br>For all reports use: ​http://localhost:8080/assignment4/trafficcamerareports/reports"
				+ "<br>For a specific report use:"
				+ "<br>http://localhost:8080/assignment4/trafficcamerareports/reports/{camera_id}";		
	}
	
	private boolean exists(String value) {
		return (value != null && !value.equals(""));
	}
	
	private boolean allExist(Report reportToAdd) {
		return exists(reportToAdd.getAtdLocationId()) &&
				exists(reportToAdd.getCameraMfg()) &&
				exists(reportToAdd.getCameraStatus()) &&
				exists(reportToAdd.getCouncilDistrict()) &&
				exists(reportToAdd.getLocationName()) &&
				exists(reportToAdd.getSignalEndArea());
	}

	@POST
	@Path("/reports")
	@Produces("application/xml")
	public Response addReport(Report reportToAdd) throws Exception {
		if (!allExist(reportToAdd)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Report newReport = this.trafficCameraReportsService.addReport(reportToAdd);
		return Response.ok(newReport, MediaType.APPLICATION_XML_TYPE).build();
	}
	
	@PUT
	@Path("/reports/{camera_id}")
	@Produces("application/xml")
	public Response updateReport(@PathParam("camera_id") int cameraId, Report updatedReport) throws Exception {
		if (!allExist(updatedReport)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Report updatedR = this.trafficCameraReportsService.updateReport(updatedReport, cameraId);
		if (updatedR == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(updatedR, MediaType.APPLICATION_XML).build();
	}

	@GET
	@Path("/reports/{camera_id}")
	@Produces("application/xml")
	public Response getReport(@PathParam("camera_id") int cameraId) throws Exception {
		Report report = this.trafficCameraReportsService.getReport(cameraId);
		if (report == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(report, MediaType.APPLICATION_XML).build();
	}
	
	@DELETE
	@Path("/reports/{camera_id}")
	@Produces("application/xml")
	public Response deleteReport(@PathParam("camera_id") int cameraId) throws Exception {
		if (this.trafficCameraReportsService.deleteReport(cameraId)) {
			return Response.ok().build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@GET
	@Path("/reports")
	@Produces("application/xml")
	public Response getAllReports(@QueryParam("camera_status") String cameraStatus) throws Exception {

		final Reports reports = new Reports();
		List<Report> reportList;
		if (cameraStatus == null || cameraStatus.equals("")) {
			reportList = this.trafficCameraReportsService.getAllReports();
		} else {
			reportList = this.trafficCameraReportsService.getAllReportsByStatus(cameraStatus);
		}
		reports.setReports(reportList);		
			    
		return Response.ok(reports, MediaType.APPLICATION_XML).build();    
	}	
}