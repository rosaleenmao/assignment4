package assign.resources;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.*;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import assign.domain.Report;


public class TestTrafficCameraReportsResource
{

   private HttpClient client;
   private static Report testReport;
   private static boolean setupDone;

   @Before
   public void initClient()
   {
	   client = HttpClientBuilder.create().build();
	   if(!setupDone) {
	   testReport = new Report();
	   testReport.setLocationName(" ROSS RD / SAINT THOMAS DR");
	   testReport.setCameraStatus("TURNED_ON");
	   testReport.setCameraMfg("Sarix");
	   testReport.setAtdLocationId("LOC16-004705");
	   testReport.setSignalEndArea("SOUTHEAST");
	   testReport.setCouncilDistrict("2");
	   setupDone = true;
	   }
   }
   
   //POST ​http://localhost:8080/assignment4/trafficcamerareports/reports/
   @Test
   public void test1Post() throws Exception
   {
      System.out.println("*** Testing addReport ***");
      
      JAXBContext jaxbContext = JAXBContext.newInstance(Report.class);
	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      
      String url = "http://localhost:8080/assignment4/trafficcamerareports/reports/";
      HttpPost request = new HttpPost(url);
      String xml = "<report>" + 
      		"<location_name> ROSS RD / SAINT THOMAS DR</location_name>" + 
      		"<camera_status>TURNED_ON</camera_status>" + 
      		"<camera_mfg>Sarix</camera_mfg>" + 
      		"<atd_location_id>LOC16-004705</atd_location_id>" + 
      		"<signal_eng_area>SOUTHEAST</signal_eng_area>" + 
      		"<council_district>2</council_district>" + 
      		"</report>";
      StringEntity entity = new StringEntity(xml);
      entity.setContentType("application/xml");
      request.setEntity(entity);

      HttpResponse response = client.execute(request);
      assertEquals(200, response.getStatusLine().getStatusCode());
      
      Report dbReport = (Report) jaxbUnmarshaller.unmarshal(response.getEntity().getContent());
      
      testReport.setCameraId(dbReport.getCameraId());
      
      assertEquals(testReport.getAtdLocationId(), dbReport.getAtdLocationId());
      assertEquals(testReport.getCameraMfg(), dbReport.getCameraMfg());
      assertEquals(testReport.getCameraStatus(), dbReport.getCameraStatus());
      assertEquals(testReport.getCouncilDistrict(), dbReport.getCouncilDistrict());
      assertEquals(testReport.getLocationName(), dbReport.getLocationName());
      assertEquals(testReport.getSignalEndArea(), dbReport.getSignalEndArea());
      
      System.out.println("*** addReport good ***");
   } 
   
   //PUT ​http://localhost:8080/assignment4/trafficcamerareports/reports/{id}
   @Test
   public void test2Put() throws Exception
   {
      System.out.println("*** Testing updateReport ***");
      
      JAXBContext jaxbContext = JAXBContext.newInstance(Report.class);
	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

      String url = "http://localhost:8080/assignment4/trafficcamerareports/reports/" + testReport.getCameraId();
      HttpPut request = new HttpPut(url);
      
      String xml = "<report>" + 
        		"<location_name> ROSS RD / SAINT THOMAS DR</location_name>" + 
        		"<camera_status>TURNED_ON</camera_status>" + 
        		"<camera_mfg>Sarix</camera_mfg>" + 
        		"<atd_location_id>LOC16-004705</atd_location_id>" + 
        		"<signal_eng_area>SOUTHEAST</signal_eng_area>" + 
        		"<council_district>4</council_district>" + 
        		"</report>";
      testReport.setCouncilDistrict("4");
      
      StringEntity entity = new StringEntity(xml);
      entity.setContentType("application/xml");
      request.setEntity(entity);

      HttpResponse response = client.execute(request);
      assertEquals(200, response.getStatusLine().getStatusCode());
      
      Report dbReport = (Report) jaxbUnmarshaller.unmarshal(response.getEntity().getContent());
      
      assertEquals(testReport.getAtdLocationId(), dbReport.getAtdLocationId());
      assertEquals(testReport.getCameraId(), dbReport.getCameraId());
      assertEquals(testReport.getCameraMfg(), dbReport.getCameraMfg());
      assertEquals(testReport.getCameraStatus(), dbReport.getCameraStatus());
      assertEquals(testReport.getCouncilDistrict(), dbReport.getCouncilDistrict());
      assertEquals(testReport.getLocationName(), dbReport.getLocationName());
      assertEquals(testReport.getSignalEndArea(), dbReport.getSignalEndArea());

      System.out.println("*** updateReport good ***");
   }
   
   //PUT ​http://localhost:8080/assignment4/trafficcamerareports/reports/{id}
   @Test
   public void test3Get() throws Exception
   {
      System.out.println("*** Testing getReport ***");
      
      JAXBContext jaxbContext = JAXBContext.newInstance(Report.class);
	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      
      String url = "http://localhost:8080/assignment4/trafficcamerareports/reports/" + testReport.getCameraId();
      HttpGet request = new HttpGet(url);

      HttpResponse response = client.execute(request);
      assertEquals(200, response.getStatusLine().getStatusCode());
      
      Report dbReport = (Report) jaxbUnmarshaller.unmarshal(response.getEntity().getContent());
      
      assertEquals(testReport.getAtdLocationId(), dbReport.getAtdLocationId());
      assertEquals(testReport.getCameraId(), dbReport.getCameraId());
      assertEquals(testReport.getCameraMfg(), dbReport.getCameraMfg());
      assertEquals(testReport.getCameraStatus(), dbReport.getCameraStatus());
      assertEquals(testReport.getCouncilDistrict(), dbReport.getCouncilDistrict());
      assertEquals(testReport.getLocationName(), dbReport.getLocationName());
      assertEquals(testReport.getSignalEndArea(), dbReport.getSignalEndArea());

      System.out.println("*** getReport good ***");
   }
   
   //GET ​http://localhost:8080/assignment4/trafficcamerareports/reports/{id}
   @Test
   public void test4Delete() throws Exception
   {
      System.out.println("*** Testing deleteReport ***");
      
      String url = "http://localhost:8080/assignment4/trafficcamerareports/reports/" + testReport.getCameraId();
      HttpDelete request = new HttpDelete(url);

      HttpResponse response = client.execute(request);
      assertEquals(200, response.getStatusLine().getStatusCode());

      System.out.println("*** deleteReport good ***");
   }
}
